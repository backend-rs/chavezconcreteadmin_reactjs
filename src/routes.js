import Dashboard from "views/Pages/Dashboard";
import Customers from "views/Pages/Customers";
import Managers from "views/Pages/Managers";
import Operators from "views/Pages/Operators";
import LoginPage from "views/Pages/LoginPage";
import UpdateUserPage from "views/Pages/UpdateUser";
import OtpPage from "views/Pages/OtpPage";
import OtpVerifyPage from "views/Pages/OtpVerifyPage";
import ForgotPasswordPage from "views/Pages/ForgotPasswordPage";
import PendingSchedulesPage from "views/Pages/PendingSchedules";
import CompletedSchedulesPage from "views/Pages/CompletedSchedules";
import ScheduleDetailsPage from "views/Pages/ScheduleDetails";
import CreateUserPage from "views/Pages/CreateUser";

var routes = [
  {
    path: "/dashboard",
    layout: "/admin",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    collapse: true,
    path: "/components",
    name: "User Management",
    state: "openComponents",
    icon: "pe-7s-users",
    views: [
      {
        path: "/customers",
        layout: "/user",
        name: "Customer",
        mini: "C",
        icon: "pe-7s-users",
        component: Customers
      },
      {
        path: "/managers",
        layout: "/user",
        name: "Manager",
        mini: "M",
        icon: "pe-7s-users",
        component: Managers
      },
      {
        path: "/operators",
        layout: "/user",
        name: "Operator",
        mini: "O",
        icon: "pe-7s-users",
        component: Operators
      },
      {
        redirect: true,
        path: "/create",
        layout: "/user",
        name: "Add New User",
        mini: "O",
        icon: "pe-7s-users-add",
        component: CreateUserPage
      },
      {
        redirect: true,
        path: "/edit-user/:id",
        layout: "/admin",
        name: "Update User",
        component: UpdateUserPage
      },
    ]
  },
  {
    collapse: true,
    path: "/schedules",
    name: "Schedule Management",
    state: "openSchedules",
    icon: "pe-7s-note2 pe-fw",
    views: [
      {
        path: "/schedules/pending",
        layout: "/admin",
        name: "Pending Schedules",
        mini: "P",
        icon: "pe-7s-menu",
        component: PendingSchedulesPage
      },
      {
        path: "/schedules/completed",
        layout: "/admin",
        name: "Completed Schedules",
        mini: "C",
        icon: "pe-7s-menu",
        component: CompletedSchedulesPage
      },
    ]
  },
  {
    redirect: true,
    path: "/schedules/details",
    layout: "/admin",
    name: "Schedule Detalis",
    component: ScheduleDetailsPage
  },

  {
    redirect: true,
    path: "/login-page",
    layout: "/auth",
    name: "Login Page",
    mini: "LP",
    component: LoginPage
  },
  {
    redirect: true,
    path: "/otp-page",
    layout: "/auth",
    name: "Otp Page",
    mini: "OTPP",
    component: OtpPage
  },
  {
    redirect: true,
    path: "/verify-otp",
    layout: "/auth",
    name: "Otp Verify Page",
    mini: "OVP",
    component: OtpVerifyPage
  },
  {
    redirect: true,
    path: "/forgot-password",
    layout: "/auth",
    name: "forgot password Page",
    mini: "Forgot",
    component: ForgotPasswordPage
  },

];
export default routes;
