import axios from "axios";

export const getData = (url, accessToken) => {
  return new Promise((resolve, reject) => {
    axios.defaults.headers.common["x-access-token"] = accessToken;
    axios
      .get(url)
      .then(response => {
        if (response !== null) {
          let data = response.data;

          if (data !== null && Object.keys(data).length !== 0) {
            if (data.statusCode === 200) {
              resolve(data);
            }
          }
        } else {
          reject(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};

export const postData = (url, AuthToken, body, headers) => {
  return new Promise((resolve, reject) => {
    axios.defaults.headers.common["x-access-token"] = AuthToken;
    axios
      .post(url, body, { headers: headers })
      .then(response => {
        console.log("Post Api data response", response);
        if (response !== null) {
          let data = response.data;
          console.log("data", data);
          if (data !== null && Object.keys(data).length !== 0) {
            if (data.statusCode === 200) {
              resolve(data);
            }
          }
        } else {
          reject(response);
        }
      })
      .catch(error => {
        console.log(error)
        reject(error.response);
      });
  });
};

export const putData = (url, AuthToken, body, headers) => {
  return new Promise((resolve, reject) => {
    axios
      .put(url, body, { headers: headers })
      .then(response => {
        console.log("heyyyyyyyyyyyyyyyyyyyyyyyyyy")

        if (response !== null) {
          let data = response.data;
          if (data !== null && Object.keys(data).length !== 0) {
            if (data.statusCode === 200) {
              resolve(data);
            }
          }
        } else {
          reject(response);
        }
      })
      .catch(error => {
        reject(error.response);
      });
  });
};
export const uploadImageApi = (url, AuthToken, body, headers) => {
  return new Promise((resolve, reject) => {
    axios.defaults.headers.common["x-access-token"] = AuthToken;
    axios
      .put(url, body, { headers: headers })
      .then(response => {
        console.log("uploadImageApi Api data response", response)
        if (response !== null) {
          let data = response.data;
          if (data !== null && Object.keys(data).length !== 0) {
            if (data.statusCode === 200 || data.statusCode === 303) {
              console.log("in")
              resolve(data);
            }
          }
        } else {
          reject(response);
        }
      })
      .catch(error => {
        reject(error);
        console.log("imageUploadAPIError", error.response)
      });
  });
};
