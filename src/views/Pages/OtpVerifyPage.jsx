import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import OtpInput from 'react-otp-input';
import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';
import * as OtpVerify from "../../../src/store/commonApi";
import * as Url from "../../../src/constants/api";

class OtpVerifyPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }

  verifyotp = () => {
    console.log('otp........', this.state)
    let otpVerifyToken = localStorage.getItem('otpVerifyToken')
    if (this.state.otp === '') {
      return alert('Please fill otp first')
    }
    if (this.state.otp.length < 4) {
      return alert('Please fill correct 4 digit otp')
    }

    let otp = this.state.otp
    const options = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }
    // console.log('token', token)
    OtpVerify.putData(Url.base + Url.otpVerify + `?otp=${otp}&otpVerifyToken=${otpVerifyToken}`, '', '', options)
      .then(response => {
        console.log("OtpVerifyPage::res", response);
        this.setState({
          dataTable: response.items,
          isLoading: false
        });
        if (response.isSuccess === true) {
          alert(response.message);
          hashHistory.push('/auth/forgot-password')
        } else {
          alert(response.error);
        }
      })
      .catch(error => {
        console.log('Errorrrrrr:', error.data.error)
        alert(error.data.error)
        this.setState({
          isLoading: false
        })
      });
  }
  render() {
    const email = this.props.location && this.props.location.state ? this.props.location.state.email : null
    let title = email ? `Please type the OTP sent to   <${email}>` : "Please check your email for OTP"
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form style={{ marginTop: "35%" }}>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                content={
                  <div>
                    <h4 style={{ color: '#B61925', fontSize: 21, fontWeight: 'bold', textAlign: "center" }}>{title}</h4>
                    <h4 style={{ color: 'red', fontSize: 15, justifyContent: 'center' }}>Note : OTP is valid for next 3 minutes only.</h4>
                    <OtpInput
                      onChange={otp => this.setState({ otp: otp })}
                      containerStyle={{ justifyContent: 'center' }}
                      inputStyle={{
                        width: " 3rem",
                        textAlign: "center",
                        height: "3rem",
                        margin: "0px 1rem",
                        fontSize: "2rem",
                        borderRadius: "4px",
                        border: "1px solid rgba(0, 0, 0, 0.3)",
                      }}
                      focusStyle={{ color: "red" }}
                      numInputs={4}
                      separator={<span>-</span>}
                      focusStyle={{ color: "black" }}
                      shouldAutoFocus={true}
                      value={this.state.otp}
                    />
                  </div>
                }

                legend={
                  <Button style={{ backgroundColor: '#B61925', borderColor: "#FFF", marginBottom: "10%" }} onClick={this.verifyotp} bsStyle="info" fill wd>
                    Verify OTP
                  </Button>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default OtpVerifyPage;
