import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";
import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from "react-router";
import * as Login from "../../store/commonApi";
import * as Url from "../../constants/api";

class LoginPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }
  forgotPassword() {
    hashHistory.push("/auth/otp-page");
  }
  login = () => {
    this.setState({
      isLoading: true
    });
    if (this.state.email === "" || this.state.password === "") {
      return alert("All fields are required");
    }
    var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!pattern.test(String(this.state.email).toLowerCase())) {
      return alert("Please Enter Valid Email");
    } else {
      let data = {
        email: this.state.email.toLowerCase(),
        password: this.state.password
      };
      const options = {
        headers: {
          "Content-Type": "application/json"
        }
      };

      Login.postData(Url.base + Url.login, "", data, options)
        .then(res => {
          this.setState({
            isLoading: false
          });
          if (res.isSuccess === true) {
            console.log("Login:response", res)
            console.log("token.......", res.data.token);
            localStorage.setItem("token", res.data.token);
            if (res.data.role === 'admin') {
              hashHistory.push("/admin/dashboard");
            } else {
              alert("Authorization failed");
            }
          } else {
            alert(res.error);
          }
        })
        .catch(error => {
          console.log('Errorrrrrr:', error)
          alert(error.data.error)
          this.setState({
            isLoading: false
          })
        });
    }
  };
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                content={
                  <div>
                    <h4
                      style={{
                        color: "#b61925",
                        fontSize: 30,
                        fontWeight: "bold",
                        textAlign: "center"
                      }}
                    >
                      Login
                    </h4>
                    <FormGroup>
                      <ControlLabel>Email address</ControlLabel>
                      <FormControl
                        placeholder="Enter email"
                        type="email"
                        onChange={event =>
                          this.setState({ email: event.target.value })
                        }
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Password</ControlLabel>
                      <FormControl
                        placeholder="Password"
                        type="password"
                        autoComplete="off"
                        onChange={event =>
                          this.setState({ password: event.target.value })
                        }
                      />
                    </FormGroup>
                  </div>
                }
                legend={
                  <div>
                    <Button
                      style={{
                        backgroundColor: "##b61925",
                        borderColor: "#FFF"
                      }}
                      onClick={this.login}
                      bsStyle="info"
                      fill
                      wd
                    >
                      Login
                    </Button>
                    {/* <NavLink to={"/auth/otp-page"} style={{ border: "none", marginLeft: "60%", color: "#B61925", fontWeight: "400" }} className="nav-link">
                      <p>Forgot Password</p>
                    </NavLink> */}
                    <Button
                      variant="link"
                      style={{
                        border: "none",
                        marginLeft: "60%",
                        color: "#b61925",
                        fontWeight: "bold",
                        textDecoration: "underline"
                      }}
                      onClick={this.forgotPassword}
                    >
                      Forgot Password
                    </Button>
                  </div>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default LoginPage;
