import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";
import { NavLink } from "react-router-dom";
import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import * as Otp from "../../../src/store/commonApi";
import * as Url from "../../../src/constants/api";
class OtpPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }
  sendotp = () => {
    console.log('detail', this.state)
    const { history } = this.props;
    if (this.state.email === '') {
      return alert('Please enter your registered email.')
    }
    var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!pattern.test(String(this.state.email).toLowerCase())) {
      return alert("Please Enter Valid Email");
    }
    else {

      let data = {
        email: this.state.email.toLowerCase(),
      }
      const options = {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }
      }
      Otp.putData(Url.base + Url.sendOtp, '', data, options)
        .then(response => {
          console.log("sendOtp::res", response);
          this.setState({
            dataTable: response.items,
            isLoading: false
          });
          if (response.isSuccess == true) {
            alert(response.message);
            localStorage.setItem('otpVerifyToken', response.data.otpVerifyToken)
            history.push({ pathname: '/auth/verify-otp', state: { email: data.email } })
          } else {
            alert(response.data.error);
          }
        })
        .catch(error => {
          console.log('Errorrrrrr:', error.data.error)
          alert(error.data.error)
          this.setState({
            isLoading: false
          })
        });
    }

  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form style={{ marginTop: "35%" }}>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                content={
                  <div>
                    <NavLink to={"/auth/login-page"} style={{ border: "none", color: "#B61925", fontWeight: "400" }} className="nav-link">
                      <i className="fa fa-arrow-left" />
                    </NavLink>
                    <h4 style={{ color: '#B61925', fontSize: 24, fontWeight: 'bold', textAlign: "center" }}>Did you forgot your password?</h4>
                    <h4 style={{ color: 'grey', fontSize: 15, fontWeight: 'bold' }}>Enter your email address you're using for your account below and we will send you a otp.</h4>
                    <FormGroup>
                      <ControlLabel>Email address</ControlLabel>
                      <FormControl placeholder="Enter email" type="email" onChange={event => this.setState({ email: event.target.value })} />
                    </FormGroup>
                    {/* <h4 style={{ color: 'grey', fontSize: 12, fontWeight: 'bold' }}>A 4 digit otp will be send to your email.Please click below to have your otp.</h4> */}
                  </div>
                }

                legend={
                  <div>
                    <Button style={{ backgroundColor: '#B61925', borderColor: "#FFF", marginBottom: "10%" }} onClick={this.sendotp} bsStyle="info" fill wd>
                      Send OTP
                  </Button>
                  </div>
                }

                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default OtpPage;
