import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormControl,
  ControlLabel,
  FormGroup
} from "react-bootstrap";
import Select from "react-select";
import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from "react-router";
import * as API from "../../store/commonApi";
import * as Url from "../../constants/api";
import LoadingOverlay from "react-loading-overlay";

// let data
class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      mobile: "",
      address: "",
      userType: "",
      equipments: [],
      equipment:'',
      isLoading: false
    };
  }

  componentDidMount() {
    this.setRole()
    this.getEquipments()
  }
  getEquipments = async () => {
    console.log('inside get equipments');
    let token = localStorage.getItem("token");
    this.setState({
      isLoading: true
    });
    try {
      let response = API.getData(`http://93.188.167.68:9100/api/equipments`, token)
      response.then(res => {
        this.setState({
          isLoading: false
        });
        if (res.data) {
          console.log('getEquipments::res', res);
          let equipmentList = []
          res.data.forEach(equipment => {
            equipmentList.push({
              label: equipment.name,
              value: equipment.id
            })
          });
          this.setState({
            equipments: equipmentList,
          })
          console.log('equipmentList',this.state.equipments)
        }
        else {
          console.log('res.error', res.error)
          // Alert.alert('', res.error)
        }

      }).catch(error => {
        this.setState({
          isLoading: false
        })
        alert("Something went wrong")
      })

    }
    catch (err) {
      this.setState({
        isLoading: false
      })
      alert("Something went wrong")
    }
  }
  setRole = async () => {
    if (this.props.location.state && this.props.location.state.type) {
      localStorage.setItem("userType", this.props.location.state.type);
    }
    let userType = localStorage.getItem("userType");
    await this.setState({
      userType: userType
    });
    console.log("userType", this.state.userType)
  }

  Cancel() {
    let userType = localStorage.getItem("userType");
    console.log(`/user/${userType}s`)
    hashHistory.push(`/user/${userType}s`);
  }

  Create(data) {
    console.log("data........", data)
    if (!data.name || !data.email || !data.mobile || !data.address) {
      return alert("All fields are mandatory");
    }
    this.setState({
      isLoading: true
    });
    let userDetail = {
      name: data.name,
      email: data.email,
      phoneNo: data.mobile,
      role: data.userType,
      address: data.address,
      assignedEquipmentId: data.equipment.value
    };
    console.log("userCreate::req", userDetail)
    const options = {
      headers: {
        "Content-Type": "application/json"
      }
    };

    API.postData(Url.base + Url.userCreate, "", userDetail, options)
      .then(res => {
        console.log("userCreate::res", res.data);
        if (res.isSuccess === true) {
          this.setState({
            isLoading: false
          });
          alert('User Created Successfully');
          let userType = localStorage.getItem("userType");
          hashHistory.push(`/user/${userType}s`);
        } else {
          alert(res.error);
        }
      })
      .catch(error => {
        this.setState({
          isLoading: false
        });
        console.log("userCreateErr", error);
        alert(error.data.error);
      });
  }


  render() {
    console.log('equipment',this.state.equipment)
    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text="Loading, please wait..."
      >
        <div>
          <Grid>
            <Row>
              {/* <Col md={9} mdOffset={4}> */}
              <Col md={11}>

                <div className="header-text">
                  <hr />
                </div>
              </Col>

              <Col md={11}>
                <form >
                  <Card
                    // plain
                    content={
                      <div>
                        <div className="row">
                          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{}}>
                            <FormGroup>
                              <ControlLabel>Name</ControlLabel>
                              {/* this.setState({ [e.target.name]: e.target.value }); */}
                              <FormControl
                                type="text"
                                name="Name"
                                placeholder="Name"

                                onChange={event =>
                                  this.setState({ name: event.target.value })
                                }
                                value={this.state.name}
                              />
                            </FormGroup>
                            <FormGroup>
                              <ControlLabel>Address</ControlLabel>
                              {/* this.setState({ [e.target.name]: e.target.value }); */}
                              <FormControl
                                type="text"
                                name="Address"
                                placeholder="Address"
                                onChange={event =>
                                  this.setState({ address: event.target.value })
                                }
                                value={this.state.address}
                              />
                            </FormGroup>

                            <FormGroup>
                              <ControlLabel>Email</ControlLabel>
                              <FormControl
                                type="text"
                                name="Email"
                                placeholder="Email"
                                onChange={event =>
                                  this.setState({ email: event.target.value })
                                }
                                value={this.state.email}
                              />
                            </FormGroup>

                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{}}>
                            <FormGroup>
                              <ControlLabel>Mobile No.</ControlLabel>
                              <FormControl
                                type="text"
                                name="Mobile"
                                placeholder="Mobile"
                                onChange={event =>
                                  this.setState({ mobile: event.target.value })
                                }
                                value={this.state.mobile}
                              />
                            </FormGroup>
                            {!this.state.userType ? <div>
                              <label style={{
                                fontWeight: '400',
                                color: '#444',
                                textTransform: 'uppercase',
                                fontSize: '12px',
                                marginBottom: '5px'
                              }}>User Type</label>
                              <Select
                                className="react-select info"
                                classNamePrefix="react-select"
                                placeholder="Choose Operator"
                                name="multipleSelect"
                                closeMenuOnSelect={true}
                                value={this.state.userType}
                                onChange={value =>
                                  this.setState({ userType: value })
                                }
                                options={[
                                  {
                                    value: "",
                                    isDisabled: true
                                  },
                                  { value: "customer", label: "Customer" },
                                  { value: "operator", label: "Operator" },
                                  { value: "manager", label: "Manager" }]}
                              />
                            </div> :
                              <FormGroup>
                                <ControlLabel>User Type</ControlLabel>
                                <FormControl
                                  readOnly={true}
                                  type="text"
                                  name="userType"
                                  placeholder="userType"
                                  onChange={event =>
                                    this.setState({ userType: event.target.value })
                                  }
                                  value={this.state.userType}
                                />
                              </FormGroup>}
                            {this.state.userType.toLowerCase() === 'operator' ? <div>
                              <label style={{
                                fontWeight: '400',
                                color: '#444',
                                textTransform: 'uppercase',
                                fontSize: '12px',
                                marginBottom: '5px'
                              }}>Equipment</label>
                              <Select
                                className="react-select info"
                                classNamePrefix="react-select"
                                placeholder="Assign Equipment"
                                name="multipleSelect"
                                closeMenuOnSelect={true}
                                value={this.state.equipment}
                                onChange={value =>
                                  this.setState({ equipment: value })
                                }
                                options={
                                  this.state.equipments}
                              />
                            </div> : null}
                          </div>
                        </div>
                      </div>
                    }
                    ftTextCenter
                    legend={
                      <div>
                        <Button
                          style={{
                            backgroundColor: "#b61925",
                            borderColor: "#FFF",
                            // marginBottom: "5%",
                            marginRight: "1%"
                          }}
                          onClick={this.Cancel}
                          bsStyle="info"
                          fill
                          wd
                        >
                          Cancel
                    </Button>
                        <Button
                          style={{
                            backgroundColor: "#b61925",
                            borderColor: "#FFF",
                            // marginBottom: "10%"
                          }}
                          onClick={() => {
                            this.Create(this.state);
                          }}
                          bsStyle="info"
                          fill
                          wd
                        >
                          Create

                    </Button>
                      </div>
                    }
                  />
                </form>
              </Col>
              <Col lg={11}>

                <div >
                  <hr />
                </div>
              </Col>
            </Row>
          </Grid>
        </div>
      </LoadingOverlay>
    );
  }
}

export default RegisterPage;
