import React, { Component } from "react";
import { Grid, Col, Row } from "react-bootstrap";
import ChartistGraph from "react-chartist";
import Card from "components/Card/Card.jsx";
import StatsCard from "components/Card/StatsCard.jsx";
import LoadingOverlay from "react-loading-overlay";
import { optionsBar, responsiveBar, table_data } from "variables/Variables.jsx";
import * as Count from "../../store/commonApi";
import * as Url from "../../constants/api";

let token;
let data = [];

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counts: {},
      userData: {
        labels: [],
        series: [[]]
      },
      operatorData: {
        labels: [],
        series: [[]]
      },
      mangerData: {
        labels: [],
        series: [[]]
      },
      customerData: {
        labels: [],
        series: [[]]
      },
      isLoading: false
    };
  }

  componentDidMount() {
    token = localStorage.getItem("token");
    this.setState({
      isLoading: true
    });

    Count.getData(Url.base + Url.counts + "/all", token)
      .then(res => {
        console.log("counst", res.data.count);
        this.getMonthlyOperatorCount();
        this.getMonthlyMangerCount();
        this.getMonthlyCustomerCount();
        console.log("res.data.count", res.data.count)
        this.setState({
          counts: res.data.count,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        console.log("countsErr", err);
      });
  }

  getMonthlyOperatorCount() {
    fetch("http://93.188.167.68:9100/api/counts/monthly/operator", {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": token
      }
    })
      .then(response => response.json())
      .then(response => {
        if (response.isSuccess === true) {
          console.log("MonthlyOperatorCount:data", response.data);
          this.setState({
            operatorData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          });
          data.push({ operatorData: this.state.operatorData });
        } else {
          alert(response.error);
        }
      })
      .catch(error => {
        console.log("Errorrrrrr:", error.message);
        alert(error.message);
        this.setState({
          isLoading: false
        });
      });
  }

  getMonthlyMangerCount() {
    fetch("http://93.188.167.68:9100/api/counts/monthly/manger", {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": token
      }
    })
      .then(response => response.json())
      .then(response => {
        if (response.isSuccess === true) {
          console.log("userMonthMangerApi:data", response.data);
          response.data.series[0] = 1;
          this.setState({
            mangerData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          });
          data.push({ mangerData: this.state.mangerData });
        } else {
          alert(response.error);
        }
      })
      .catch(error => {
        console.log("Errorrrrrr:", error.message);
        alert(error.message);
        this.setState({
          isLoading: false
        });
      });
  }

  getMonthlyCustomerCount() {
    fetch("http://93.188.167.68:9100/api/counts/monthly/customer", {
      method: "get",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "x-access-token": token
      }
    })
      .then(response => response.json())
      .then(response => {
        if (response.isSuccess === true) {
          console.log("userMonthCustomerApi:data", response.data);
          this.setState({
            isLoading: false,
            customerData: {
              labels: response.data.labels,
              series: [response.data.series]
            }
          });
          data.push({ customerData: this.state.customerData });
          console.log(".....data....", data)
        } else {
          console.log("Errorrrrrr:", response.error);
        }
      })
      .catch(error => {
        console.log("Errorrrrrr:", error);
        this.setState({
          isLoading: false
        });
      });
  }

  createTableData() {
    var tableRows = [];
    for (var i = 0; i < table_data.length; i++) {
      tableRows.push(
        <tr key={i}>
          <td>
            <div className="flag">
              <img src={table_data[i].flag} alt="us_flag" />
            </div>
          </td>
          <td>{table_data[i].country}</td>
          <td className="text-right">{table_data[i].count}</td>
          <td className="text-right">{table_data[i].percentage}</td>
        </tr>
      );
    }
    return tableRows;
  }
  render() {
    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text="Loading, please wait..."
      >
        <div className="main-content">
          <Grid fluid>
            <Row>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-users text-warning" />}
                  statsText="Operator"
                  statsValue={this.state.counts.totalOperator || 0}
                />
              </Col>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-users text-success" />}
                  statsText="Customer"
                  statsValue={this.state.counts.totalCustomer || 0}
                />
              </Col>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-users text-success" />}
                  statsText="Manager"
                  statsValue={this.state.counts.totalManger || 0}
                />
              </Col>
              <Col lg={3} sm={6}>
                <StatsCard
                  bigIcon={<i className="pe-7s-users text-success" />}
                  statsText="Total"
                  statsValue={this.state.counts.totalCount}
                // statsIcon={<i className="fa fa-refresh" />}
                // statsIconText="Updated now"
                />
              </Col>
            </Row>
            <Row></Row>
            <Row>
              <Col md={6}>
                <Card
                  title="Customer"
                  category=""
                  content={
                    <ChartistGraph
                      data={this.state.customerData}
                      type="Line"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                // stats={
                //   <div>
                //     <i className="fa fa-check" /> Data information certified
                //   </div>
                // }
                />
              </Col>
              <Col md={6}>
                <Card
                  title="Manager"
                  category=""
                  content={
                    <ChartistGraph
                      data={this.state.mangerData}
                      type="Line"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                // stats={
                //   <div>
                //     <i className="fa fa-check" /> Data information certified
                //   </div>
                // }
                />
              </Col>
            </Row>
            <Row>
              <Col md={6}>
                <Card
                  title="Operator"
                  category=""
                  content={
                    <ChartistGraph
                      data={this.state.operatorData}
                      type="Line"
                      options={optionsBar}
                      responsiveOptions={responsiveBar}
                    />
                  }
                // stats={
                //   <div>
                //     <i className="fa fa-check" /> Data information certified
                //   </div>
                // }
                />
              </Col>
            </Row>
          </Grid>
        </div>
      </LoadingOverlay>
    );
  }
}

export default Dashboard;
