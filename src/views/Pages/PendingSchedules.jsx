import React, { Component } from "react";
import ReactTable from "react-table";
import Card from "components/Card/Card.jsx";
// react component used to create charts
import Button from "components/CustomButton/CustomButton.jsx";
// import Loader from 'react-loader-spinner'
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as scheduleApi from "../../store/commonApi";
import * as Url from "../../constants/api";

class Managers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      jobs: [],
      data: []
    };
    // this.update(dataTable);
  }

  update(dataTable) {
    this.setState({
      isLoading: false,
      data: dataTable.map((item, key) => {
        // console.log("prop", item);
        let time = item.time === 'Invalid date' ? '' : `/${item.time}`
        return {
          isLoading: false,
          scheduleNo: item.scheduleNo,
          id: item.id,
          dateTime: `${item.date}${time.toUpperCase()}`,
          customerName: item.customer.name.charAt(0).toUpperCase() + item.customer.name.slice(1),
          jobAddress: item.projectAddress.charAt(0).toUpperCase() + item.projectAddress.slice(1) || "--",
          phoneNo: item.customer.phoneNo || "--",
          status: item.jobStatus.charAt(0).toUpperCase() + item.jobStatus.slice(1),
          actions: (
            // we've added some custom button actions
            <div className="actions-left">
              {/* use this button to add a edit kind of action */}
              <Button
                onClick={() => {
                  let obj = this.state.data;

                  obj.find((item, i) => {
                    if (i === key) {
                      const { history } = this.props;
                      console.log("scheduleId", item.id);
                      localStorage.setItem("scheduleId", item.id);
                      history.push({
                        pathname: `/admin/schedules/details/${item.id}`,
                        state: { data: item }
                      });
                    }
                  });
                }}
                bsStyle="danger"
                simple
                icon
              >
                <i className="fa fa-edit" />
              </Button>{" "}
              {/* use this button to remove the data row */}
            </div>
          )
        };
      })
    });
  }

  componentDidMount() {
    this.setState({
      isLoading: true
    });
    this.getPendingJobs();
  }

  getPendingJobs = async (duration) => {
    console.log('geting pending jobs for duration', duration)
    // this.setState({ activeTab: duration })
    let token = localStorage.getItem("token");
    let status = 'pending';
    duration = 'all'
    this.setState({
      isLoading: true
    })


    try {
      scheduleApi.getData(Url.base + Url.scheduleList + `?role=admin&status=${status}&duration=${duration}`, token)
        .then(res => {
          if (res.isSuccess) {
            console.log('res', res);
            this.setState({
              jobs: res.data,
              isLoading: false
            })
            this.update(res.data);
          }
          else {
            alert('', res.error)
          }

        }).catch(err => {
          this.setState({
            isLoading: false
          })
          // alert(err.data.error);
          // alert('something went wrong');
          console.log('inside catch of res', err);
        })

    }
    catch (err) {
      this.setState({
        isLoading: false
      })
      console.log('inside catch of try', err);
      alert("Something went wrong")
    }

  }
  render() {
    console.log("dataTable", this.state.jobs);

    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text="Loading, please wait..."
      >
        <div className="main-content">
          <Card
            // title="DataTables.net"
            content={
              <ReactTable
                data={this.state.data}
                // filterable
                columns={[
                  {
                    Header: "Schedule No",
                    accessor: "scheduleNo"
                  },
                  {
                    Header: "ARRIVAL DATE/TIME",
                    accessor: "dateTime"
                  },
                  {
                    Header: "Customer Name",
                    accessor: "customerName"
                  },
                  {
                    Header: "Job Address",
                    accessor: "jobAddress"
                  },
                  {
                    Header: "Phone Number",
                    accessor: "phoneNo"
                  },
                  {
                    Header: "Status",
                    accessor: "status"
                  },

                  {
                    Header: "Actions",
                    accessor: "actions",
                    sortable: false,
                    filterable: false
                  }
                ]}
                defaultPageSize={5}
                showPaginationTop={true}
                showPaginationBottom={false}
                className="-striped -highlight"
              />
            }
          />
        </div>
      </LoadingOverlay>
    );
  }
}

export default Managers;
