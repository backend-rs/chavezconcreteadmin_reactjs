import React, { Component } from "react";
import ReactTable from "react-table";
import Card from "components/Card/Card.jsx";
// react component used to create charts
import CustomButton from "components/CustomButton/CustomButton.jsx";
// import Loader from 'react-loader-spinner'
import LoadingOverlay from "react-loading-overlay";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import * as UserList from "../../store/commonApi";
import * as Url from "../../constants/api";
import Switch from "react-bootstrap-switch";
import { Container, Button, Link } from 'react-floating-action-button'
class Managers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTable: [],
      data: [],
      bg_checked: false,
    };
    // this.update(dataTable);
  }
  openCreateUserPage(type) {
    this.props.history.push({
      pathname: `/user/create/${type}`,
      state: { type: type }
    });
  }
  update(dataTable) {
    this.setState({
      isLoading: false,
      data: dataTable.map((prop, key) => {
        console.log("prop", prop);
        return {
          isLoading: false,
          id: prop.userId,
          _id: prop.id,
          name: prop.name.charAt(0).toUpperCase() + prop.name.slice(1),
          email: prop.email,
          phoneNo: prop.phoneNo || "--",
          status: prop.status || "not define",
          bg_checked: prop.status === 'active' ? true : false,
          actions: (
            // we've added some custom button actions
            <div className="actions-left">
              <div className="pull-left">
                <Switch
                  onChange={() => this.onUpdateStatus(prop)}
                  value={prop.status === 'active' ? true : false}
                  onText={'active'}
                  offText={'inactive'}
                  offColor={'#b61925'}
                />
              </div>
              {" "}
              {/* use this button to add a edit kind of action */}
              <CustomButton
                onClick={() => {
                  let obj = this.state.data;

                  obj.find((item, i) => {
                    if (i === key) {
                      const { history } = this.props;
                      console.log("userID", item.id);
                      history.push({
                        pathname: `/admin/edit-user/${item._id}`,
                        state: { data: item }
                      });
                    }
                  });
                }}
                bsStyle="danger"
                simple
                icon
              >
                <i className="fa fa-edit" />
              </CustomButton>{" "}
            </div>
          )
        };
      })
    });
  }
  onUpdateStatus = async (user) => {
    let status
    if (user.status === 'active') {
      status = 'inactive'
    } else {
      status = 'active'
    }
    console.log(user.id)
    this.setState({
      bg_checked: !this.state.bg_checked,
      status: status
    });
    const options = {
      headers: {
        "Content-Type": "application/json"
      }
    };
    let token = localStorage.getItem("token");
    if (user.id != undefined && user.id != null) {
      UserList.putData(
        Url.base + Url.user + `/${user.id}/update/${status}`,
        token,
        {},
        options
      )
        .then(res => {
          console.log("updetedUserStatus", res.data);
          alert(res.data);
          this.getUsers()
        })
        .catch(err => {
          this.setState({
            isLoading: false
          });
          console.log("userupdateErr", err);
        });
    } else {
      alert("somthing went wrong");
    }

  }
  componentDidMount() {
    this.setState({
      isLoading: true
    });
    this.getUsers();
  }

  getUsers() {
    let token = localStorage.getItem("token");
    console.log("tokenusers", token);
    this.setState({
      isLoading: true
    });
    UserList.getData(Url.base + Url.userListByRole + `?role=manager`, token)
      .then(res => {
        console.log("getUserList", res);
        this.setState({
          dataTable: res.data,
          isLoading: false
        });
        this.update(res.data);
      })
      .catch(err => {
        this.setState({
          isLoading: false
        });
        // alert(err.data.error);
        alert('something went wrong');
        // console.log("userListErr", err.data.error);
      });
  }

  render() {
    console.log("dataTable", this.state.dataTable);

    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text="Loading, please wait..."
      >
        <div className="main-content">
          <Card
            // title="DataTables.net"
            content={<div>
              <div>
                <ReactTable
                  data={this.state.data}
                  // filterable
                  columns={[
                    {
                      Header: "Id",
                      accessor: "id"
                    },
                    {
                      Header: "Name",
                      accessor: "name"
                    },
                    {
                      Header: "Email",
                      accessor: "email"
                    },
                    {
                      Header: "PhoneNo",
                      accessor: "phoneNo"
                    },
                    {
                      Header: "Status",
                      accessor: "status"
                    },

                    {
                      Header: "Actions",
                      accessor: "actions",
                      sortable: false,
                      filterable: false
                    }
                  ]}
                  defaultPageSize={5}
                  showPaginationTop={true}
                  showPaginationBottom={false}
                  className="-striped -highlight"
                />
              </div>
              <div className="row">
                <div className="col" style={{
                  bottom: '8vh',
                  position: 'absolute',
                  // margin: '0.7em',
                  right: '0.5vw',
                }}>
                  <Button
                    icon="pe-7s-add-user"
                    tooltip="Add New Manager"
                    styles={{
                      backgroundColor: '#b61925', color: 'white', fontSize: "25px", width: '45px',
                      height: '45px'
                    }}
                    onClick={() => this.openCreateUserPage('manager')}
                  />
                </div>
              </div>
            </div>
            }
          />
        </div>
      </LoadingOverlay>
    );
  }
}

export default Managers;
