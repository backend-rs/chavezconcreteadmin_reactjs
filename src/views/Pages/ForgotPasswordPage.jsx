import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';
import * as ForgotPassword from "../../../src/store/commonApi";
import * as Url from "../../../src/constants/api";
class ForgotPasswordPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      confirmPassword: '',
      cardHidden: true
    };
  }
  componentDidMount() {
    setTimeout(
      function () {
        this.setState({ cardHidden: false });
      }.bind(this),
      700
    );
  }

  setPassword = () => {
    if (!this.state.newPassword || !this.state.confirmPassword) {
      alert('Please all fields');
    }
    if (this.state.newPassword !== this.state.confirmPassword) {
      alert('Passwords do not match');
    } else {
      try {
        let otpVerifyToken = localStorage.getItem('otpVerifyToken')
        let data = {
          newPassword: this.state.newPassword,
          otpVerifyToken: otpVerifyToken,
        }
        const options = {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          }
        }
        ForgotPassword.putData(Url.base + Url.forgotPassword, '', data, options)
          .then(response => {
            console.log("sendOtp::res", response);
            this.setState({
              dataTable: response.items,
              isLoading: false
            });
            if (response.isSuccess === true) {
              alert(response.message);
              localStorage.removeItem('otpVerifyToken')
              hashHistory.push('/auth/login-page')
            } else {
              alert(response.error);
            }
          })
          .catch(error => {
            console.log('Errorrrrrr:', error.data.error)
            alert(error.data.error)
            this.setState({
              isLoading: false
            })
          });
      } catch (error) {
        console.log('error', error)
      }
    }


  }
  render() {
    return (
      <Grid>
        <Row>
          <Col md={4} sm={6} mdOffset={4} smOffset={3}>
            <form style={{ marginTop: "35%" }}>
              <Card
                hidden={this.state.cardHidden}
                textCenter
                content={
                  <div>
                    <h4 style={{ color: '#B61925', fontSize: 21, fontWeight: 'bold', textAlign: "center" }}>Set Password</h4>
                    <FormGroup>
                      <ControlLabel>New Password</ControlLabel>
                      <FormControl placeholder="New Password" type="password" autoComplete="off" onChange={event => this.setState({ newPassword: event.target.value })}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Confirm Password</ControlLabel>
                      <FormControl placeholder="Confirm Password" type="password" autoComplete="off" onChange={event => this.setState({ confirmPassword: event.target.value })}
                      />
                    </FormGroup>
                  </div>
                }

                legend={
                  <Button style={{ backgroundColor: '#B61925', borderColor: "#FFF", marginBottom: "10%" }} onClick={this.setPassword} bsStyle="info" fill wd>
                    SUBMIT
                  </Button>
                }
                ftTextCenter
              />
            </form>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default ForgotPasswordPage;
