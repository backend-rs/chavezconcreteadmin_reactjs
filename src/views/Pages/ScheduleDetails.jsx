import React, { Component } from "react";
import { Grid, Row, Col, FormControl, ControlLabel, FormGroup } from "react-bootstrap";
import Card from "components/Card/Card.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from "react-router";
import * as Schedule from "../../store/commonApi";
import * as Url from "../../constants/api";
import LoadingOverlay from "react-loading-overlay";
import Select from "react-select";
// let data
class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      customerName: "",
      phoneNo: "",
      date: "",
      time: "",
      jobStatus: "",
      projectAddress: "",
      scheduleNo: "",
      threeBy10System: 0,
      fourBy10System: 0,
      fiveBy10System: 0,
      mudsnake: false,
      wallpipe: false,
      isLoading: false,
      additinalInfo: '',
      equipmentName: '',
      operatorList: [],
      operator: ''
    };
  }

  Cancel() {
    hashHistory.push("/admin/schedules");
  }

  getSchedule = () => {
    let token = localStorage.getItem("token");
    let scheduleId = localStorage.getItem("scheduleId");
    this.setState({
      isLoading: true
    });
    // const scheduleId = this.props.match.params.id;
    if (scheduleId !== undefined && scheduleId !== null) {
      console.log(Url.base + Url.scheduleById + `/${scheduleId}`)
      Schedule.getData(
        Url.base + Url.scheduleById + `/${scheduleId}`,
        token
      ).then(res => {
        console.log("ScheduleDetails::getSchedule", res.data);
        this.setState({
          id: res.data.id,
          customerName: res.data.customer.name,
          phoneNo: res.data.customer.phoneNo,
          date: res.data.date,
          time: res.data.time,
          jobStatus: res.data.jobStatus,
          projectAddress: res.data.projectAddress,
          scheduleNo: res.data.scheduleNo,
          threeBy10System: res.data.threeBy10System,
          fourBy10System: res.data.fourBy10System,
          fiveBy10System: res.data.fiveBy10System,
          mudsnake: res.data.mudsnake,
          wallpipe: res.data.wallpipe,
          additinalInfo: res.data.additinalInfo,
          equipmentName: res.data.equipment.name,
          isLoading: false
        });
        if (res.data.jobStatus === 'pending') {
          this.getOperatorByEquipment(res.data.equipment.id)
        }
      })
        .catch(err => {
          this.setState({
            isLoading: false
          });
          console.log("scheduleListErr", err);
        });
    } else {
      alert("somthing went wrong");
    }
  };

  getOperatorByEquipment = async (equipmentId) => {
    let token = localStorage.getItem("token");
    console.log('authToken in operators equipmentId', equipmentId)
    // this.setState({
    //   isLoading: true
    // })
    try {
      console.log('in try')
      Schedule.getData(
        Url.base + Url.getOperatorByEquipmentId + `?equipmentId=${equipmentId}`,
        token
      ).then(res => {
        this.setState({
          isLoading: false
        })

        if (res.isSuccess) {
          console.log('getOperators :res ', res);
          let operators = []
          operators.push({
            value: "",
            isDisabled: true
          })
          res.items.forEach(item => {
            let object = { "value": item.id, "label": item.name }
            // console.log("In for loop", object)

            operators.push(object)

          });
          this.setState({
            operatorList: operators,
          })
          console.log('getOperators :operators ', this.state.operatorList);

        }
        else {
          alert('', res.error)
        }

      }).catch(error => {
        this.setState({
          isLoading: false
        })
        alert("Something went wrong")
      })

    }
    catch (err) {
      this.setState({
        isLoading: false
      })
      alert("Something went wrong")
    }
  }

  assignOperator = async () => {
    let token = localStorage.getItem("token");
    this.setState({
      isLoading: true
    })
    let body = {
      scheduleId: this.state.id,
      operatorId: this.state.operator.value
    }
    // console.log("assignOPerator::body", body)
    try {

      const options = {
        headers: {
          "Content-Type": "application/json"
        }
      };
      Schedule.putData(
        Url.base + Url.assignOperator,
        token,
        body,
        options
      ).then(res => {
        this.setState({
          isLoading: false
        })
        console.log('success', res)

        if (res.isSuccess) {
          this.setState({
            isLoading: false,
          })
          alert(res.message);
        }
        else {
          alert(res.error);
        }

      }).catch(error => {
        this.setState({
          isLoading: false
        })
        console.log('error in try', error)
        alert(error.data.message)
      })
    }
    catch (err) {
      this.setState({
        isLoading: false
      })
      console.log('error in catch', err)
      alert("Something went wrong")
    }

  }
  componentDidMount() {
    this.getSchedule();
  }

  render() {
    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text="Loading, please wait..."
      >
        <div>
          <Grid>
            <Row>
              {/* <Col md={9} mdOffset={4}> */}
              <Col md={11}>
                <div >
                  <h2 style={{ textAlign: "center", color: "red" }}></h2>
                  <hr />
                </div>
              </Col>
              <Col md={11}>
                <form>
                  <Card
                    // plain
                    content={
                      <div>
                        {this.state.jobStatus === 'pending' ?

                          <div className="row" style={{ marginBottom: "2%" }}>
                            <div className="col-lg-2 col-md-2 col-sm-2 col-sm-2" style={{}}>
                              {/* <label style={{ color: "#b61925", fontSize: "x-large", marginLeft: "5%" }}>ASSIGN OPERATOR TO SCHEDULE</label> */}
                              <label style={{ color: "#b61925", paddingTop: "5%", fontSize: 'medium' }}>ASSIGN OPERATOR</label>
                            </div>
                            <div className="col-lg-4 col-md-4 col-sm-4 col-sm-4">
                              <Select
                                className="react-select info"
                                classNamePrefix="react-select"
                                placeholder="Choose Operator"
                                name="multipleSelect"
                                closeMenuOnSelect={false}
                                // isMulti
                                value={this.state.operator}
                                onChange={value =>
                                  this.setState({ operator: value })
                                }
                                options={this.state.operatorList}
                              />
                            </div>
                          </div> : ""}

                        <div className="row">
                          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{}}>
                            <FormGroup>
                              <ControlLabel>Customer Name</ControlLabel>
                              {/* this.setState({ [e.target.name]: e.target.value }); */}
                              <FormControl
                                type="text"
                                readOnly={true}
                                name="customerName"
                                placeholder="customerName"
                                onChange={event =>
                                  this.setState({ customerName: event.target.value })
                                }
                                value={this.state.customerName}
                              />
                            </FormGroup>
                            <FormGroup>
                              <ControlLabel>Arrival Date</ControlLabel>
                              <FormControl
                                type="text"
                                readOnly={true}
                                name="Date"
                                placeholder="Date"
                                onChange={event =>
                                  this.setState({ date: event.target.value })
                                }
                                value={this.state.date}
                              />
                            </FormGroup>
                            <FormGroup>
                              <ControlLabel>Job Address</ControlLabel>
                              {/* this.setState({ [e.target.name]: e.target.value }); */}
                              <FormControl
                                type="textarea"
                                rows="5"
                                readOnly={true}
                                name="Address"
                                placeholder="Address"
                                onChange={event =>
                                  this.setState({ projectAddress: event.target.value })
                                }
                                value={this.state.projectAddress}
                              />
                            </FormGroup>
                          </div>
                          <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{}}>

                            <FormGroup>
                              <ControlLabel>Contact Number</ControlLabel>
                              {/* this.setState({ [e.target.name]: e.target.value }); */}
                              <FormControl
                                type="text"
                                readOnly={true}
                                name="PhoneNo"
                                placeholder="PhoneNo"
                                onChange={event =>
                                  this.setState({ phoneNo: event.target.value })
                                }
                                value={this.state.phoneNo}
                              />
                            </FormGroup>

                            <FormGroup>
                              <ControlLabel>Start Time</ControlLabel>
                              <FormControl
                                readOnly={true}
                                type="text"
                                name="Time"
                                placeholder="Time"
                                onChange={event =>
                                  this.setState({ time: event.target.value })
                                }
                                value={this.state.time}
                              />
                            </FormGroup>
                            <div>
                              <FormGroup>
                                <ControlLabel>Status</ControlLabel>
                                <FormControl
                                  readOnly={true}
                                  type="text"
                                  name="jobStatus"
                                  placeholder="jobStatus"
                                  onChange={event =>
                                    this.setState({ jobStatus: event.target.value })
                                  }
                                  value={this.state.jobStatus}
                                />
                              </FormGroup>

                            </div>
                          </div>
                        </div>
                      </div>
                    }
                    ftTextCenter
                  />
                </form>
              </Col>
              <Col md={11}>

                <form>
                  <Card
                    // plain
                    content={
                      <div className="row">
                        <div>
                          <label style={{ marginLeft: "1.4%", fontSize: "x-large" }}>Equipment Details</label>
                        </div><div className="col-lg-12">
                          <label style={{ marginLeft: "1.4%", fontSize: "xx-large", color: '#d5442b', marginLeft: '40%' }}>{this.state.equipmentName}</label>
                        </div>
                        {/* <label style={{ textAlign: "center" }}>Equipment Details:</label> */}

                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{}}>
                          <FormGroup>
                            {/* <ControlLabel>{this.state.equipmentName}</ControlLabel> */}

                            <ControlLabel>3" x 10' Systems</ControlLabel>
                            {/* this.setState({ [e.target.name]: e.target.value }); */}
                            <FormControl
                              type="text"
                              readOnly={true}
                              name="threeBy10System"
                              placeholder="threeBy10System"
                              onChange={event =>
                                this.setState({ threeBy10System: event.target.value })
                              }
                              value={this.state.threeBy10System}
                            />
                          </FormGroup>
                          <FormGroup>
                            <ControlLabel>4" x 10' Systems</ControlLabel>
                            <FormControl
                              type="text"
                              readOnly={true}
                              name="fourBy10System"
                              placeholder="fourBy10System"
                              onChange={event =>
                                this.setState({ fourBy10System: event.target.value })
                              }
                              value={this.state.fourBy10System}
                            />
                          </FormGroup>
                          <FormGroup>
                            <ControlLabel>5" x 10' Systems</ControlLabel>
                            {/* this.setState({ [e.target.name]: e.target.value }); */}
                            <FormControl
                              type="textarea"
                              rows="5"
                              readOnly={true}
                              name="fiveBy10System"
                              placeholder="fiveBy10System"
                              onChange={event =>
                                this.setState({ fiveBy10System: event.target.value })
                              }
                              value={this.state.fiveBy10System}
                            />
                          </FormGroup>
                        </div>
                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6" style={{}}>
                          <FormGroup>
                            <ControlLabel>Wallpipe</ControlLabel>
                            {/* this.setState({ [e.target.name]: e.target.value }); */}
                            <FormControl
                              type="text"
                              readOnly={true}
                              name="wallpipe"
                              placeholder="wallpipe"
                              onChange={event =>
                                this.setState({ wallpipe: event.target.value })
                              }
                              value={this.state.wallpipe}
                            />
                          </FormGroup>

                          <FormGroup>
                            <ControlLabel>Mudsnake</ControlLabel>
                            <FormControl
                              readOnly={true}
                              type="text"
                              name="mudsnake"
                              placeholder="mudsnake"
                              onChange={event =>
                                this.setState({ mudsnake: event.target.value })
                              }
                              value={this.state.mudsnake}
                            />
                          </FormGroup>
                          <FormGroup>
                            <ControlLabel>Job Condition & Notes</ControlLabel>
                            <FormControl
                              readOnly={true}
                              type="text"
                              name="additinalInfo"
                              placeholder="additinalInfo"
                              onChange={event =>
                                this.setState({ additinalInfo: event.target.value })
                              }
                              value={this.state.additinalInfo}
                            />
                          </FormGroup>
                        </div>
                      </div>
                    }
                    ftTextCenter
                    legend={

                      <div className="row">
                        {this.state.jobStatus === 'pending' ?
                          <div className="col-sm-12">
                            <Button
                              style={{
                                // marginTop: "4%",
                                borderColor: "#b61925",
                                backgroundColor: "#FFF",
                                // marginBottom: "10%",
                                color: "#b61925",
                                fontSize: "large",
                                fontWeight: "5%"
                              }}
                              onClick={this.assignOperator}
                              bsStyle="info"
                              fill
                              wd
                            >
                              ASSIGN
                  </Button>
                          </div> : ''
                        }
                      </div>
                    }
                  />
                </form>
              </Col>
            </Row>
          </Grid>
        </div>
      </LoadingOverlay >
    );
  }
}

export default RegisterPage;
