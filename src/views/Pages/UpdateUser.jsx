import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormControl,
  ControlLabel,
  FormGroup
} from "react-bootstrap";
import Switch from "react-bootstrap-switch";

import Card from "components/Card/Card.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from "react-router";
import * as User from "../../store/commonApi";
import * as Url from "../../constants/api";
import LoadingOverlay from "react-loading-overlay";

// let data
class RegisterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      id: "",
      mobile: "",
      status: "",
      address: "",
      profilePic: "",
      bg_checked: false,
      file: null,
      isLoading: false,
      userType: ''
    };
    this.onChange = this.onChange.bind(this);
  }

  Cancel() {
    let userType = localStorage.getItem("userType");
    console.log("userType.......", userType)
    hashHistory.push(`/user/${userType}s`);
  }

  Update(data) {
    console.log("data........", data)
    if (!data.name || !data.email || !data.mobile || !data.address) {
      return alert("All fields are mandatory");
    }
    let userDetail = {
      name: data.name,
      email: data.email,
      phoneNo: data.mobile,
      status: data.status,
      address: data.address,
    };
    const options = {
      headers: {
        "Content-Type": "application/json"
      }
    };
    let token = localStorage.getItem("token");
    this.setState({
      isLoading: true
    });
    const userId = this.props.match.params.id;
    if (userId != undefined && userId != null) {
      User.putData(
        Url.base + Url.user + `/update/${userId}`,
        token,
        userDetail,
        options
      )
        .then(res => {
          console.log("updetedUser", res.data);
          this.setState({
            email: res.data.email || "",
            name: res.data.name || "",
            phoneNo: res.data.phoneNo || "",
            status: res.data.status || "",
            bg_checked: res.data.status === 'active' ? true : false,
            address: res.data.address || "",
            isLoading: false
          });
          alert("user update succesfully");
          // this.update(res.data);
        })
        .catch(err => {
          this.setState({
            isLoading: false
          });
          console.log("userupdateErr", err);
        });
    } else {
      alert("somthing went wrong");
    }
  }

  updateProfilePic = async (e) => {
    e.preventDefault();
    this.setState({
      isLoading: true
    })
    const options = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }
    var formdata = new FormData();
    console.log("e.target.files[0]", e.target.files[0])
    formdata.append('file', e.target.files[0]);
    let token = localStorage.getItem("token");
    const userId = this.props.match.params.id;
    try {
      User.uploadImageApi(
        Url.base + Url.uploadProfilePic + `?id=${userId}`,
        token,
        formdata,
        options
      ).then(res => {
        console.log("testaajjdadj", res)
        this.setState({
          isLoading: false
        })
        if (res.data) {
          console.log('profile pic res', res.data)
          // AsyncStorage.getItem('token', res.data.token)
          this.setState({
            isLoading: false,
            profilePic: res.data.profilePic,
          })
          alert("ProfilePic Uploaded Successfully")

        }
        else {
          // Alert.alert('', res.error)
        }

      }).catch(error => {
        this.setState({
          isLoading: false
        })
        console.log('error in try', error)
        alert("Something went wrong ")
      })

    }
    catch (err) {
      this.setState({
        isLoading: false
      })
      console.log('error in catch', err)
      alert("Something went wrong")
    }
  }

  getUser = () => {
    let token = localStorage.getItem("token");
    console.log("getuser", token);
    console.log("this.props.match.params", this.props.match.params.id);


    this.setState({
      isLoading: true
    });
    const userId = this.props.match.params.id;
    if (userId != undefined && userId != null) {
      User.getData(
        Url.base + Url.userById + `/${this.props.match.params.id}`,
        token
      )
        .then(res => {
          console.log("getUser", res.data);
          this.setState({
            email: res.data.email || "",
            name: res.data.name || "",
            mobile: res.data.phoneNo || "",
            status: res.data.status || "",
            bg_checked: res.data.status === 'active' ? true : false,
            address: res.data.address || "",
            profilePic: res.data.profilePic || "",
            isLoading: false,
            userType: res.data.role
          });
          if (res.data.role) {
            localStorage.setItem("userType", res.data.role);
          }
        })
        .catch(err => {
          this.setState({
            isLoading: false
          });
          console.log("userListErr", err);
        });
    } else {
      alert("somthing webt wrong");
    }
  };

  componentDidMount() {
    this.getUser();
  }

  onChange(e) {
    this.setState({ file: e.target.files[0] });
    this.updateProfilePic(e)
  }
  onChangeClick = () => {
    let status
    if (this.state.bg_checked) {
      status = 'inactive'
    } else {
      status = 'active'
    }
    console.log(status)
    this.setState({
      bg_checked: !this.state.bg_checked,
      status: status
    });
  }
  render() {
    return (
      <LoadingOverlay
        active={this.state.isLoading}
        spinner
        text="Loading, please wait..."
      >
        <div>
          <Grid>
            <Row>
              {/* <Col md={9} mdOffset={4}> */}
              <Col md={11}>

                <div className="header-text">
                  <h2 style={{ textAlign: "center", color: "#b61925" }}>Edit User</h2>
                  <hr />
                </div>
              </Col>

              <Col md={11}>
                <form>
                  <Card
                    // plain
                    content={
                      <div>
                        <img src={this.state.profilePic} style={{ height: "140px", width: "140px", borderRadius: "50%", borderWidth: "5%", borderColor: "black", marginLeft: "43%", marginRight: "43%" }} />
                        <input style={{ marginLeft: "43%", marginRight: "4%", marginTop: "2%" }} type="file" onChange={
                          this.onChange
                        } />
                      </div>
                    }
                  />
                  <Card
                    // plain
                    content={
                      <div>
                        <div className="pull-right">
                          {/* <p className="pull-left">Status   </p> */}
                          <Switch
                            onChange={this.onChangeClick}
                            value={this.state.bg_checked}
                            onText={'active'}
                            offText={'inactive'}
                            offColor={'#b61925'}
                          />
                        </div>
                        <FormGroup>
                          <ControlLabel>Name</ControlLabel>
                          {/* this.setState({ [e.target.name]: e.target.value }); */}
                          <FormControl
                            type="text"
                            name="Name"
                            placeholder="Name"
                            onChange={event =>
                              this.setState({ name: event.target.value })
                            }
                            value={this.state.name}
                          />
                        </FormGroup>
                        <FormGroup>
                          <ControlLabel>Address</ControlLabel>
                          {/* this.setState({ [e.target.name]: e.target.value }); */}
                          <FormControl
                            type="text"
                            name="Address"
                            placeholder="Address"
                            onChange={event =>
                              this.setState({ address: event.target.value })
                            }
                            value={this.state.address}
                          />
                        </FormGroup>

                        <FormGroup>
                          <ControlLabel>Email</ControlLabel>
                          <FormControl
                            type="text"
                            name="Email"
                            placeholder="Email"
                            onChange={event =>
                              this.setState({ email: event.target.value })
                            }
                            value={this.state.email}
                          />
                        </FormGroup>
                        <FormGroup>
                          <ControlLabel>Mobile No.</ControlLabel>
                          <FormControl
                            type="text"
                            name="Mobile"
                            placeholder="Mobile"
                            onChange={event =>
                              this.setState({ mobile: event.target.value })
                            }
                            value={this.state.mobile}
                          />
                        </FormGroup>

                      </div>
                    }
                    ftTextCenter
                    legend={
                      <div>
                        <Button
                          style={{
                            backgroundColor: "#b61925",
                            borderColor: "#FFF",
                            marginBottom: "10%"
                          }}
                          onClick={this.Cancel}
                          bsStyle="info"
                          fill
                          wd
                        >
                          Cancel
                    </Button>
                        <Button
                          style={{
                            backgroundColor: "#b61925",
                            borderColor: "#FFF",
                            marginBottom: "10%"
                          }}
                          onClick={() => {
                            this.Update(this.state);
                          }}
                          bsStyle="info"
                          fill
                          wd
                        >
                          Update

                    </Button>
                      </div>
                    }
                  />
                </form>
              </Col>
            </Row>
          </Grid>
        </div>
      </LoadingOverlay>
    );
  }
}

export default RegisterPage;
