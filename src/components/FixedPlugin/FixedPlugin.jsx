/*eslint-disable*/
import React, { Component } from "react";
import Switch from "react-bootstrap-switch";

import Button from "components/CustomButton/CustomButton.jsx";

import imagine1 from "assets/img/full-screen-image-1.jpg";
import imagine2 from "assets/img/full-screen-image-2.jpg";
import imagine3 from "assets/img/full-screen-image-5.png";
import imagine4 from "assets/img/full-screen-image-4.jpg";

class FixedPlugin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: "dropdown show-dropdown open",
      bg_checked: true,
      bgImage: this.props.bgImage,
      switched: false,
      navbar_checked: false
    };
    this.handleClick = this.handleClick.bind(this);
    this.onChangeClick = this.onChangeClick.bind(this);
    this.onNavbarClick = this.onNavbarClick.bind(this);
    this.onMiniClick = this.onMiniClick.bind(this);
  }
  handleClick() {
    this.props.handleFixedClick();
  }
  onChangeClick() {
    this.setState({ bg_checked: !this.state.bg_checked });
    this.props.handleHasImage(this.state.bg_checked);
  }
  onNavbarClick() {
    this.setState({ navbar_checked: !this.state.navbar_checked });
    this.props.handleNavbarClick(this.state.navbar_checked);
  }
  onMiniClick() {
    this.props.handleMiniClick();
  }
  render() {
    return <div className="fixed-plugin"></div>;
  }
}

export default FixedPlugin;
