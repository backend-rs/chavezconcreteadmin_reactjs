import React, { Component } from "react";
import {
  Nav,
} from "react-bootstrap";
import Button from "components/CustomButton/CustomButton.jsx";
import { hashHistory } from 'react-router';
import * as Logout from "../../store/commonApi";
import * as Url from "../../constants/api";

class HeaderLinks extends Component {
  logout() {
    let token = localStorage.getItem('token')
    const options = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }
    console.log('token', token)
    Logout.postData(Url.base + Url.logout, token, {}, options)
      .then(res => {
        this.setState({
          isLoading: false
        });
        if (res.isSuccess === true) {
          console.log("Login:response", res)
          localStorage.removeItem('token')
          hashHistory.push('/auth/login-page')
        } else {
          alert(res.error);
        }
      }).catch(error => {
        console.log('Errorrrrrr:', error)
        alert(error.data.error)
        this.setState({
          isLoading: false
        })
      })
  }
  render() {
    return (
      <div>
        <Nav pullRight>
          <div className="text-danger">
            <Button
              onClick={() => this.logout()}
              bsStyle="danger"
              simple
              icon
            >
              <i className="pe-7s-close-circle" /> Log out
              </Button>{" "}
          </div>
        </Nav>
      </div>
    );
  }
}
export default HeaderLinks;
