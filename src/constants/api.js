export const base = "http://93.188.167.68:9100/api";
export const login = "/users/login";
export const logout = "/users/logout";
export const deleteUser = "/users/delete";
export const userList = "/users/getList";
export const userById = "/users/getById";
export const scheduleById = "/schedules/getById";
export const getOperatorByEquipmentId = "/users/getOperatorList";
export const assignOperator = "/schedules/operator/assign";
export const userListByRole = "/users/getList/by/role";
export const scheduleList = "/schedules/getByStatus";
export const counts = "/counts";
export const user = "/users";
export const userCreate = "/users/register";
export const uploadProfilePic = "/users/upload/image/profilePic";
export const otpVerify = "/users/otp/verify";
export const sendOtp = "/users/otp";
export const forgotPassword = "/users/forgotPassword";
